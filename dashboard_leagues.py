# -*- coding: utf-8 -*-

# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.

import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.express as px
import pandas as pd
import glob

app = dash.Dash(__name__)

def get_data(chosen_league=None):
    df = pd.read_csv('../f1-fantasy-optimizer/files/leagues_results.csv')
    if chosen_league != None:
        desired_charts = ['team_rank','team_score','team_rank_percentile_below','score_to_champion']
        df = df[df['id'].isin([int(chosen_league)])]
        league_name = df['name'].drop_duplicates().tolist()[0]
        return df, desired_charts, league_name
    else:
        return df

def get_charts(df, desired_charts):
    figures_list = []
    for single_chart in desired_charts:
        fig = px.line(df, x="race", y=single_chart, color='picked_team_name')
        fig.update_xaxes(
            tickangle = -45
        )
        fig.update_layout(
            title={
                'text': '{} over time'.format(single_chart),
                'y': 0.9,
                'x': 0.5,
                'xanchor': 'center',
                'yanchor': 'top'
            }
        )
        
        if single_chart in ['team_rank', 'score_to_champion']:
            fig.update_yaxes(
                autorange = "reversed"
            )

        fig.update_traces(
            mode='markers+lines'
        )
        figures_list.append(
            dcc.Graph(
                id='example-graph-{}'.format(single_chart),
                figure=fig,
                style={'width': '90%'}
            )
        )
    return figures_list

def serve_layout(chosen_league=None):
    df = get_data(None)
    last_race = df['race'].drop_duplicates().tolist()[-1]
    
    dash_inputs = []
    layout_children_list = [
        html.H1(children='F1 Fantasy Leagues Dashboard', style={'text-align': 'center'}),
        html.H2(children='Last race: {}'.format(last_race), style={'text-align': 'center'})
    ]
    
    last_week_leagues = df[df['race'].isin([last_race])]
    leagues_champion_df = df[df['score_to_champion'] == 0].drop_duplicates('name')
    
    table_content = [
        html.Tr([
        html.Th('League Name', style={'border': '1px solid black', 'padding': '5px'}),
        html.Th('Rank', style={'border': '1px solid black', 'padding': '5px'}),
        html.Th('Entrants', style={'border': '1px solid black', 'padding': '5px'}),
        html.Th('Change', style={'border': '1px solid black', 'padding': '5px'}),
        html.Th('Top %', style={'border': '1px solid black', 'padding': '5px'}),
        html.Th('Top 25%', style={'border': '1px solid black', 'padding': '5px'}),
        html.Th('Top 20%', style={'border': '1px solid black', 'padding': '5px'}),
        html.Th('Top 15%', style={'border': '1px solid black', 'padding': '5px'}),
        html.Th('Top 10%', style={'border': '1px solid black', 'padding': '5px'}),
        html.Th('Top 5%', style={'border': '1px solid black', 'padding': '5px'}),
        html.Th('Top 1%', style={'border': '1px solid black', 'padding': '5px'}),
        html.Th('Top 0.1%', style={'border': '1px solid black', 'padding': '5px'}),
        html.Th('Champion', style={'border': '1px solid black', 'padding': '5px'})
    ], style={'border': '1px solid black', 'padding': '5px'})
    ]
    for single_league in last_week_leagues['name'].drop_duplicates().tolist():
        single_league_df = last_week_leagues[last_week_leagues['name'] == single_league]
        is_champion = ''
        if single_league in leagues_champion_df['name'].tolist():
            # table and is 10% is 5% is 1% is 0.1% is_champion, sortować jakoś wg % below, albo wg ilości osób w lidze (o, to spoko)
            # po wybraniu ligi, pokazywać wykresy w ukrytym divie poniżej za pomocą funkcji 
            is_champion = '👑'
        if single_league_df['team_rank_percentile_below'].median() >= 0.75 or is_champion != '':
            is_top_25 = '✔️'
        else:
            is_top_25 = ''
        if single_league_df['team_rank_percentile_below'].median() >= 0.8 or is_champion != '':
            is_top_20 = '✔️'
        else:
            is_top_20 = ''
        if single_league_df['team_rank_percentile_below'].median() >= 0.85 or is_champion != '':
            is_top_15 = '✔️'
        else:
            is_top_15 = ''
        if single_league_df['team_rank_percentile_below'].median() >= 0.9 or is_champion != '':
            is_top_10 = '✔️'
        else:
            is_top_10 = ''
        if single_league_df['team_rank_percentile_below'].median() >= 0.95 or is_champion != '':
            is_top_5 = '✔️'
        else:
            is_top_5 = ''
        if single_league_df['team_rank_percentile_below'].median() >= 0.99 or is_champion != '':
            is_top_1 = '✔️'
        else:
            is_top_1 = ''
        if single_league_df['team_rank_percentile_below'].median() >= 0.999 or is_champion != '':
            is_top_01 = '✔️'
        else:
            is_top_01 = ''

        button_name = 'button-{}'.format(single_league_df['id'].tolist()[0])
        team_diff = single_league_df['team_rank_diff'].tolist()[0]

        if team_diff > 0:
            symbol = '+'
            color = 'green'
        elif team_diff == 0:
            symbol = ''
            color='grey'
        else: 
            symbol='-'
            color = 'red'
        
        team_diff = '{}{}'.format(symbol, int(team_diff))
        top_percentage = '{}%'.format((round((100*(1-single_league_df['team_rank_percentile_below'].tolist()[0])),6)))
        table_content.append(
            html.Tr(
                [
                    html.Button(single_league_df['name'].tolist()[0], 
                        id=button_name,
                        style={
                        'border': 'none', 'background': 'none', 'color': 'inherit', 'padding': '5px', 'font': 'inherit', 'cursor': 'pointer', 'outline': 'inherit'}
                        ),
                    html.Td(single_league_df['team_rank'].tolist()[0], style={'border': '1px solid black', 'padding': '5px'}),
                    html.Td(single_league_df['entrants_count_corrrected'].tolist()[0], style={'border': '1px solid black', 'padding': '5px'}),
                    html.Td(team_diff, style={'border': '1px solid black', 'padding': '5px', 'color': color}),
                    html.Td(top_percentage, style={'border': '1px solid black', 'padding': '5px'}),
                    html.Td(is_top_25, style={'border': '1px solid black', 'padding': '5px'}),
                    html.Td(is_top_20, style={'border': '1px solid black', 'padding': '5px'}),
                    html.Td(is_top_15, style={'border': '1px solid black', 'padding': '5px'}),
                    html.Td(is_top_10, style={'border': '1px solid black', 'padding': '5px'}),
                    html.Td(is_top_5, style={'border': '1px solid black', 'padding': '5px'}),
                    html.Td(is_top_1, style={'border': '1px solid black', 'padding': '5px'}),
                    html.Td(is_top_01, style={'border': '1px solid black', 'padding': '5px'}),
                    html.Td(is_champion, style={'border': '1px solid black', 'padding': '5px'})
                ], style={'border': '1px solid black', 'padding': '5px'}
            )
        )
        dash_inputs.append(Input(button_name, 'n_clicks'))
    layout_children_list.append(html.Table(children=table_content, style={'border': '1px solid black', 'border-collapse': 'collapse', 'text-align': 'center'}))
    layout = html.Div(children=layout_children_list + [html.Br()], style={'margin': '0 auto', 'width': '40%'})
    return layout, dash_inputs

# app.layout = serve_layout('F1 Instagram Official League')
layout, dash_inputs = serve_layout()

app.layout = html.Div(children = [
    layout,
    html.Div(id='hidden_button', style={'display': 'none'}),
    html.Div(id='charts')
])

@app.callback(
    [Output(component_id='hidden_button', component_property='children')],
    dash_inputs
)
def update_hidden_div(*args):
    ctx = dash.callback_context
    if not ctx.triggered:
        button_id = 'No clicks yet'
    else:
        button_id = ctx.triggered[0]['prop_id']

    return [html.Button(button_id, id='user_choice', value=button_id)]

@app.callback(
    [Output(component_id='charts', component_property='children')],
    [Input(component_id='user_choice', component_property='value')]
)
def update_charts_div(hidden_value):
    chosen_league = hidden_value.replace('button-', '').split('.')[0]
    df = get_data(chosen_league)
    if chosen_league != None:
        df, desired_charts,league_name = df
        figures_list = get_charts(df, desired_charts)
        layout = html.Div(children=[
            html.H2(children='Chosen league: {}'.format(league_name), style={'text-align': 'center'}),
            html.Div(
                figures_list,
                style={'margin': '0 auto', 'width': '90%', 'height': '3000px'}
            )
        ])

    return [layout]

if __name__ == '__main__':
    app.run_server('0.0.0.0', port=8061, debug=False)