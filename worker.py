from datetime import datetime
import json
import pandas as pd
import glob
import os
from utils.download_data import *
from utils.commit_files import *
from utils.check_price_change import *
from utils.drivers_api import *
from utils.python_processes_running import *

current_time = str(datetime.now()).split('.')[0].replace(' ', '_')
response = download_data('https://fantasy-api.formula1.com/partner_games/f1/players')
response_object = json.loads(response.text)

last_json_file_path = sorted(glob.glob('data/json/*'))[-1]

with open(last_json_file_path) as json_file:
    old_json = json.load(json_file)

python_wrapper_running = python_processes_running(process_text = 'python3 wrapper.py')

if old_json != response.text and python_wrapper_running == False:
    with open('data/json/{}.json'.format(current_time), 'w', encoding='utf-8') as f:
        json.dump(response.text, f, ensure_ascii=False, indent=4)

    players = []
    keys_desired = [
        'price',
        'chance_of_playing'
    ]
    for single_player in response_object['players']:
        player_name = drivers_api[single_player['id']]
        
        if single_player['weekly_price_change'] == single_player['price']:
            change = 0.0
        else:
            change = single_player['weekly_price_change']
        basic_dict = {
            'name': player_name,
            'request_time': current_time
        }
        basic_dict2 = { your_key: single_player[your_key] for your_key in keys_desired }
        basic_dict.update(basic_dict2)
        basic_dict['weekly_price_change'] = change
        basic_dict.update(single_player['current_price_change_info'])
        players.append(basic_dict)

    players_df = pd.DataFrame().from_records(players)
    players_df.to_csv('data/csv/{}.csv'.format(current_time))

    git_push('add data', [
        'data/json/{}.json'.format(current_time),
        'data/csv/{}.csv'.format(current_time)
    ])

    trigger_optimizer, names = check_price_change(current_time)

    if trigger_optimizer and names is not None:
        os.system('cd ~/f1-fantasy-optimizer/; python3 execute.py --exceptional True --names "{}"'.format(names))
