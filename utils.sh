# permissions for python file
chmod 755 worker.py
chmod 755 runner.sh

# crontab content
SHELL=/bin/bash
PATH=/sbin:/bin:/usr/sbin:/usr/bin
1,11,21,31,41,51 * * * * ~/f1-fantasy-dashboard/runner.sh

