# -*- coding: utf-8 -*-

# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.

import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px
import pandas as pd
import glob

app = dash.Dash(__name__)

def get_data():
    all_files = sorted(glob.glob('data/csv/*'))

    li = []

    for filename in all_files:
        df = pd.read_csv(filename, index_col=None, header=0)
        li.append(df)

    df = pd.concat(li, axis=0, ignore_index=True)
    desired_charts = df.columns[3:]
    return df, desired_charts

def get_charts(df, desired_charts):
    figures_list = []
    for single_chart in desired_charts:
        fig = px.line(df, x="request_time", y=single_chart, color='name')
        fig.update_xaxes(
            tickangle = -45
        )
        fig.update_layout(
            title={
                'text': '{} over time'.format(single_chart),
                'y': 0.9,
                'x': 0.5,
                'xanchor': 'center',
                'yanchor': 'top'
            }
        )
        fig.update_traces(
            mode='markers+lines'
        )
        figures_list.append(
            dcc.Graph(
                id='example-graph-{}'.format(single_chart),
                figure=fig,
                style={'width': '90%'}
            )
        )
    return figures_list

def serve_layout():
    df, desired_charts = get_data()
    figures_list = get_charts(df, desired_charts)
    layout = html.Div(children=[
        html.H1(children='F1 Fantasy Dashboard', style={'text-align': 'center'}),
        html.Div(
            figures_list,
            style={'margin': '0 auto', 'width': '90%', 'height': '3000px'}
        )
    ])
    return layout

app.layout = serve_layout

if __name__ == '__main__':
    app.run_server('0.0.0.0', port=8060, debug=False)