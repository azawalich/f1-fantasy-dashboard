def initiate_secrets():
    secrets = {}

    f = open('secrets.sh', 'r')
    lines_read = f.read().splitlines()[1:]
    f.close()

    for line in lines_read:
        line_splitted = line.replace('\n', '').replace('"', '').split('=')
        secrets[line_splitted[0]] = line_splitted[1]
    
    return secrets
