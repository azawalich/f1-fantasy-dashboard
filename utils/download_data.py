import requests
from utils import cookie

def download_data(url):
    response = requests.get(
        url,
        headers={
            'x-f1-cookie-data': cookie.cookie_hash
        }
    )
    return response