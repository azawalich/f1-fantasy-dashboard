import os

def python_processes_running(process_text):
    processes = [(int(p), c) for p, c in [x.rstrip('\n').split(' ', 1) for x in os.popen('ps h -eo pid:1,command')]]
    python_processes = []
    for single_process in processes:
        p_id, p_command = single_process
        if process_text in p_command:
            python_processes.append(p_id)
    if len(python_processes) > 0:
        return True
    else:
        return False