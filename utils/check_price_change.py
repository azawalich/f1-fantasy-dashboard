import glob
import os
import pandas as pd
import utils.banned_drivers as bdr

def check_price_change(current_time):
    previous_csv_file = sorted(glob.glob('data/csv/*'))[-2]

    old_df = pd.read_csv(previous_csv_file)[['name', 'price']].rename(columns={'price': 'old_price'})
    new_df = pd.read_csv('data/csv/{}.csv'.format(current_time))[['name', 'price']]
    
    joined_df = pd.merge(new_df, old_df, on='name', how='left')
    joined_df['price_diff'] = joined_df['price'] - joined_df['old_price']

    price_diffs = joined_df[~joined_df['price_diff'].isin([0.0])]
    price_diffs_names_list = price_diffs['name'].tolist()
    price_diffs_names = ', '.join(price_diffs_names_list)

    set_intersect = set(price_diffs_names_list).intersection(set(bdr.banned_drivers))

    if price_diffs.shape[0] > 0 and len(set_intersect) != len(price_diffs_names_list):
        return True, price_diffs_names
    else:
        return False, None